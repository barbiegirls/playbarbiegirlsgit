window.onload = function(){geid('loadSpinner').style.visibility='hidden'; geid('dressup_contents').style.display='flex';};

const cats = ["skin","hair","eyes","lips","tops","sleeves","bottoms","shoes","purse","jewelry","more"];
const apiPrefix = "";
const assetMaxNum = {hair: 16, eyes: 8, lips: 5, tops: 29, sleeves: 3, bottoms: 11, shoes: 13, purse: 7, jewelry: 11, more: 8};
const transparent = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=";
var currentCategory = "";
var clubBeautySession = "";
var cbMakeoverDict = {};
const colorsDict = {
  'skin': ["255 243 230","253 220 210","246 211 176","223 176 118","204 125 54","165 105 51","133 71 14","105 51 11","138 102 68","80 40 10","116 67 48","124 57 49","191 130 85","251 191 255","254 143 195","241 103 103","253 243 104","102 223 92","93 243 165","173 240 252","59 103 214","141 96 244"],
  'hair': ["117 47 22","147 167 176","146 66 151","42 55 107","219 43 113","179 131 85","0 0 0","77 77 77","114 191 68","14 20 14","243 229 201","181 128 82","239 217 177","204 175 89","138 102 68","233 199 101","128 87 59","247 238 205","35 43 34","234 232 222","237 199 89","144 97 64","113 72 42","185 65 18","84 65 43","229 213 186","231 216 192","188 75 163"],
  'eyes': ["0 113 143","124 144 44","102 80 30","188 143 217","68 214 248","37 27 1","105 175 0","146 164 174","121 163 221","173 141 72","166 170 133","170 182 203","94 185 202","89 55 34","123 62 51","71 102 178","165 132 84","99 133 177","115 101 105","44 101 56","89 101 104","89 84 101","74 92 110","62 117 143","118 197 176","160 195 188","193 235 230","203 229 235","200 236 206","210 209 180","202 188 129","223 204 174","222 177 163","216 109 111","86 138 98","101 79 115"],
  'lips': ["197 18 76","181 56 26","198 68 75","229 56 147","250 135 178","254 211 234","114 10 99","158 10 23","219 40 114","222 177 163","216 109 111","248 188 166","167 219 225","253 227 112","255 128 52","222 225 231","247 239 193","205 177 34","250 134 241","141 61 152","131 171 173","202 170 147","203 38 41","248 233 202","255 255 255","160 157 204","251 199 215","34 26 2","177 214 137","224 151 198","118 50 37","41 52 104","37 154 150","236 110 154","37 154 150"],
  'others': ["241 122 163","224 151 198","236 110 110","252 44 169","219 40 114","243 13 105","203 38 41","255 0 0","255 128 52","255 190 56","255 245 0","253 227 112","247 239 193","205 177 34","207 255 104","161 205 140","88 148 60","31 118 56","161 246 216","108 182 186","13 38 125","0 18 178","64 116 223","136 163 218","18 185 239","167 219 225","148 255 249","141 61 152","164 109 162","125 44 146","65 41 104","203 147 227","250 134 241","222 225 231","181 176 172","34 26 2","255 255 255","118 50 37","144 97 64","202 170 147"]
};

var assetsToSkip = ['more2'];
var currentThumbsIndexes = {};
var currentColorsIndexes = {};
var placeHolderCounts = {hair: 4, eyes: 2, lips: 2, tops: 4, sleeves: 1, bottoms: 6, shoes: 2, purse: 2, jewelry: 2, more: 2};
var currentWearing = {hair: 0, eyes: 0, lips: 0, tops: 0, sleeves: 0, bottoms: 0, shoes: 0, purse: 0, jewelry: 0, more: 0};
var ColorPlaceholderCounts = {skin: 11, hair: 17, eyes: 4, lips: 4, tops: 14, sleeves: 3, bottoms: 20, shoes: 6, purse: 6, jewelry: 6, more: 6};

function geid(id){return document.getElementById(id);}
function handleImgError(imgN){imgN.src = transparent;}
function saveCache(key,val){localStorage[key] = val;}
function getCache(key){return localStorage[key];}

function initView()
{
  geid("av_head").src = apiPrefix + "html/avatar/avatar_head.png";
  geid("av_body").src = apiPrefix + "html/avatar/avatar_body.png";
  geid("av_fist").src = apiPrefix + "html/avatar/avatar_fist.png";
  geid("av_head_c").data = apiPrefix + "html/avatar/avatar_head_c.png";
  geid("av_body_c").data = apiPrefix + "html/avatar/avatar_body_c.png";
  geid("av_fist_c").data = apiPrefix + "html/avatar/avatar_fist_c.png";
  var images = geid("avcontainer").getElementsByTagName("img");
  for(let i=0; i<images.length; i++) {images[i].onerror = function() {handleImgError(this);};}    
  initCurrentThumbsIndexes('');
  initColorsOnDisplay('');
  loadCache(); //local dressup
  goToStep0_HOME();
}

function loadCache()
{
  for(let cat of cats)
  {
    let saved = getCache(cat);
    if(saved && saved!="null" && saved!='') wear(saved, cat);
    let savedColor = getCache(cat+"-color");
    if(savedColor && savedColor!="null" && savedColor!='') setCatColor(cat, savedColor);
    updateThumbnailsSelection(cat);
  }
  let savedSkinColor = getCache("skinColor");
  if(savedSkinColor && savedSkinColor!="null") setSkinColor(savedSkinColor);
}

function goToStep0_HOME()
{
  geid('step1').checked = false; geid('step2').checked = false; geid('step3').checked = false;
  geid('STEP0_HOME').style.display = 'flex'; geid('STEP0_HOME').style.visibility = 'visible';
  geid('STEP1_LOOKS').style.display = 'none'; geid('STEP1_LOOKS').style.visibility = 'hidden';
  geid('STEP2_DRESS').style.display = 'none'; geid('STEP2_DRESS').style.visibility = 'hidden';
  geid('STEP3_ACCESSORIZE').style.display = 'none'; geid('STEP3_ACCESSORIZE').style.visibility = 'hidden';
  geid('saveButton').style.visibility = 'hidden';
}

function goToStep1()
{
  geid('STEP0_HOME').style.display = 'none'; geid('STEP0_HOME').style.visibility = 'hidden';
  geid('STEP1_LOOKS').style.display = 'flex'; geid('STEP1_LOOKS').style.visibility = 'visible';
  geid('STEP2_DRESS').style.display = 'none'; geid('STEP2_DRESS').style.visibility = 'hidden';
  geid('STEP3_ACCESSORIZE').style.display = 'none'; geid('STEP3_ACCESSORIZE').style.visibility = 'hidden';
  geid('saveButton').style.visibility = 'visible';
}

function goToStep2()
{
  geid('STEP0_HOME').style.display = 'none'; geid('STEP0_HOME').style.visibility = 'hidden';
  geid('STEP1_LOOKS').style.display = 'none'; geid('STEP1_LOOKS').style.visibility = 'hidden';
  geid('STEP2_DRESS').style.display = 'flex'; geid('STEP2_DRESS').style.visibility = 'visible';
  geid('STEP3_ACCESSORIZE').style.display = 'none'; geid('STEP3_ACCESSORIZE').style.visibility = 'hidden';
  geid('saveButton').style.visibility = 'visible';
}

function goToStep3()
{
  geid('STEP0_HOME').style.display = 'none'; geid('STEP0_HOME').style.visibility = 'hidden';
  geid('STEP1_LOOKS').style.display = 'none'; geid('STEP1_LOOKS').style.visibility = 'hidden';
  geid('STEP2_DRESS').style.display = 'none'; geid('STEP2_DRESS').style.visibility = 'hidden';
  geid('STEP3_ACCESSORIZE').style.display = 'flex'; geid('STEP3_ACCESSORIZE').style.visibility = 'visible';
  geid('saveButton').style.visibility = 'visible';
}

function initCurrentThumbsIndexes(category)
{
  for(let cat of cats){
    if(category!='' && cat!=category) continue;
    currentThumbsIndexes[cat]=[];
    let num = 1;
    for(let h=1; h<=placeHolderCounts[cat]; h++){
      let aname = cat+num;
      while(assetsToSkip.includes(aname)){aname = cat+(++num);}
      currentThumbsIndexes[cat].push(num);
      num++;
    }
  }
}

function initColorsOnDisplay(cat)
{
  for(let category of cats)
  {
    if(cat!='' && cat!=category) continue;
    let colorPlaceholderCounts = ColorPlaceholderCounts[category];
    currentColorsIndexes[category] = new Array(colorPlaceholderCounts);
    for(let h=1; h<=colorPlaceholderCounts; h++){currentColorsIndexes[category][h-1] = h-1;} //starts with 0
    updateColorsSelections(category);
  }

}
function updateColorsSelections(category)
{
  let key = category;
  if(!colorsDict.hasOwnProperty(key)) key = "others";
  let colorPlaceholderCounts = ColorPlaceholderCounts[category];
  for(let h=1; h<=colorPlaceholderCounts; h++)
  {
    let color = colorsDict[key][currentColorsIndexes[category][h-1]];
    let acolordiv = geid(category+"_Color"+h);
    acolordiv.style.backgroundColor = "rgb("+color+")";
    acolordiv.onclick = function(){clickedCatColor(category,this.style.backgroundColor);};
  }
}

function clickedNextAsset(category)
{
  if(currentThumbsIndexes[category][placeHolderCounts[category]-1] == assetMaxNum[category]) //rightmost thumb on display is the last asset
  {
    initCurrentThumbsIndexes(category);
    geid(category+"_ButtonPrev").style.visibility='hidden'; //already to the max asset num,initialized-> hide prev button
  }
  else
  {
    geid(category+"_ButtonPrev").style.visibility='visible';
    for(let i=1; i<=placeHolderCounts[category]; i++)
    {
      let curNum = currentThumbsIndexes[category][i-1];
      let nextNum = curNum+1;
      while(assetsToSkip.includes(category+nextNum)) nextNum++;
      currentThumbsIndexes[category][i-1] = nextNum;
    }
  }
  updateThumbnails(category);
}

function clickedPrevAsset(category)
{
  for(let i=1; i<=placeHolderCounts[category]; i++)
  {
    let curNum = currentThumbsIndexes[category][i-1];
    let nextNum = curNum-1;
    while(assetsToSkip.includes(category+nextNum)) nextNum--;
    currentThumbsIndexes[category][i-1] = nextNum;
  }
  if(currentThumbsIndexes[category][0] == 1) 
  {
    //initCurrentThumbsIndexes(category);
    geid(category+"_ButtonPrev").style.visibility='hidden'; //already to the min asset num-> hide prev button
  }
  updateThumbnails(category);
}

function clickedNextColor(category)
{
  let key = category;
  if(!colorsDict.hasOwnProperty(key)) key = "others";
  if(currentColorsIndexes[category][ColorPlaceholderCounts[category]-1] == colorsDict[key].length-1)  
  {
    initColorsOnDisplay(category);
    geid(category+"_cButtonPrev").style.visibility='hidden'; //already to the max asset num,initialized-> hide prev button
  }
  else
  {
    geid(category+"_cButtonPrev").style.visibility='visible';
    for(let i=1; i<=ColorPlaceholderCounts[category]; i++)
    {
      let curNum = currentColorsIndexes[category][i-1]; 
      currentColorsIndexes[category][i-1] = curNum+1;
    }
  }
  updateColorsSelections(category);
}
function clickedPrevColor(category)
{
  for(let i=1; i<=ColorPlaceholderCounts[category]; i++)
  {
    let curNum = currentColorsIndexes[category][i-1];
    currentColorsIndexes[category][i-1] = curNum-1;
  }
  if(currentColorsIndexes[category][0] == 0) 
  {
    //initColorsOnDisplay(category);
    geid(category+"_cButtonPrev").style.visibility='hidden'; //already to the min asset num-> hide prev button
  }
  updateColorsSelections(category);
}

function updateThumbnails(category)
{
  for(let i=1; i<=placeHolderCounts[category]; i++)
  {
    let assetNameToLoad = category+currentThumbsIndexes[category][i-1];
    geid('thumbs_'+category+i).src = transparent;
    geid('thumbs_'+category+i).src = "html/thumbnails/"+category+"_thumbnails/"+assetNameToLoad+".png";
  }
  updateThumbnailsSelection(category);
}
function updateThumbnailsSelection(category)
{
  for(let i=1; i<=placeHolderCounts[category]; i++)
  {
    let thumbAssetName = category+currentThumbsIndexes[category][i-1];
    let vis = 'hidden';
    if(currentWearing[category]==thumbAssetName) vis = 'visible';
    geid('thumbsDash_'+category+i).style.visibility = vis;
  }
}

function clickedWear(thumbIndex, category)
{
  let filename = category+currentThumbsIndexes[category][thumbIndex];
  if(currentWearing[category]==filename) unwear(category);
  else wear(filename, category);
  updateThumbnailsSelection(category);
}

function wear(filename, category)
{
  if(filename=="") return;
  let defaultImg = geid(category).childNodes.item(0);
  let colorableCanvas = geid(category).childNodes.item(1);
  let textureImg = geid(category).childNodes.item(2);
  defaultImg.src = apiPrefix + "html/"+category+"/"+filename+".png";
  colorableCanvas.data = apiPrefix + "html/"+category+"/"+filename+"c.png";
  colorableCanvas.style.visibility = 'hidden'; //wear default color
  textureImg.src = apiPrefix + "html/"+category+"/"+filename+"t.png";
  geid(category).style.visibility = 'visible';
  currentWearing[category] = filename; currentWearing[category+"-color"] = '';
  if(category=="hair") wear(filename.replace("hair","hairback"), "hairback");
}

function clickedCatColor(category, color)
{
  if(category==null || category=="") return;
  if(category=='skin') {setSkinColor(color); return;}
  if(geid(category).style.visibility == 'hidden') return;
  setCatColor(category, color);
}

function setCatColor(category, color)
{
  if(color=="") return;
  if(category==null || category=="") return;
  if(geid(category).style.visibility == 'hidden') return;
  let colorableCanvas = geid(category).childNodes.item(1);
  let defaultImg = geid(category).childNodes.item(0);
  setColor(defaultImg,colorableCanvas,color);
  currentWearing[category+"-color"] = color;
  if(category=="hair") setCatColor("hairback", color);
}

function setSkinColor(color)
{
  setColor(geid("av_head"),geid("av_head_c"),color);
  setColor(geid("av_body"),geid("av_body_c"),color);
  setColor(geid("av_fist"),geid("av_fist_c"),color);
  currentWearing["skinColor"] = color;
}

function setColor(defaultImg,colorableCanvas, color)
{
  let img=new Image();
  img.onload = function () 
  {
    let dHeight = defaultImg.height;
    let dWidth = defaultImg.width;
    colorableCanvas.height = dHeight;
    colorableCanvas.width = dWidth;
    let ctx = colorableCanvas.getContext('2d'); 
    ctx.drawImage(img,0,0,dWidth,dHeight); //draw original colorable img
    let c=document.createElement('canvas');
    c.width=img.width;
    c.height=img.height;
    let cctx=c.getContext('2d');
    cctx.drawImage(img,0,0,dWidth,dHeight);
    cctx.globalCompositeOperation='source-atop';
    cctx.fillStyle=color;
    cctx.fillRect(0,0,img.width,img.height); //draw colorable img with full new color to canvas c
    ctx.globalCompositeOperation='multiply';
    ctx.drawImage(c,0,0); // draw the fully colored canvas c on top of the grayscale image
    ctx.globalCompositeOperation='source-over';
    colorableCanvas.style.visibility = 'visible';
  }
  img.src = colorableCanvas.data; //reset to original colorable img before set new color

}

function unwear(currentCategory)
{
  if(currentCategory==null || currentCategory=="") return;
  geid(currentCategory).style.visibility = 'hidden';
  geid(currentCategory).childNodes.item(1).style.visibility = 'hidden';
  currentWearing[currentCategory] = '';
  if(currentCategory=="hair")
  {
    geid("hairback").style.visibility = 'hidden';
    geid("hairback").childNodes.item(1).style.visibility = 'hidden';
    currentWearing["hairback"] = '';
  }
  currentWearing[currentCategory] = '';
}

function saveOutfit(){for(let key in currentWearing) saveCache(key, currentWearing[key]); goToStep0_HOME();}

initView();
